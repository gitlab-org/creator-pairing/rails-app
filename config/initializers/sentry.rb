if Rails.env.production?
  Raven.configure do |config|
    config.dsn = 'https://5add24f96d3042cb84ba0ba5235368ad@o282080.ingest.sentry.io/5289212'
    config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
  end
end
